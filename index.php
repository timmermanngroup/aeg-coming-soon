<!DOCTYPE html>
<html lang="en">

<head>
    <title>Aligned Equity Group</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Calling Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Calling Favicon -->
    <!-- ********************* -->
    <!-- CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/color.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/default.css">
    <!-- End CSS files -->
</head>

<body>
    <!-- Preloader start -->
    <div class="loader-page flex-center">
        <img src="img/loader.gif" alt="">
    </div>
    <!-- Preloader end -->

    <!-- coming soon area start -->
    <section class="coming-soon pt-50 flex-center-cmng pb-100 vh-100" data-overlay="9">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 offset-lg-1">
                    <div class="coming-soon-outer z-5">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <img class="logo-img" src="img/alig_logo_primary.png">
                                <h1 class="f-700 mb-10 fs-55 lh-11">Coming <span class="green">Soon</span></h1>

                                <div class="hr-2 bg-black opacity-1 mt-30 mb-30"></div>

                            </div>

                        </div>


                        <div class="row justify-content-center">

                            <div class="col-lg-6 text-center text-lg-left">
                                
                                <p class="mb-30">Please enter your email along with your question or comment for AEG here and a representative will respond to you soon.</p>
                            </div>

                            <div class="col-lg-6 text-center">
                                <?php if(isset($_COOKIE['ContactSuccess'])):?>
                                    <div class="mb-3">
                                        <h3 class="green">Message sent</h3>
                                    </div>
                                    <?php setcookie("ContactSuccess", "", time() - 3600, "/"); ?>
                                <?php else: ?>
                                    <form action="./assets/form-script.php" method="POST">
                                        <div class="mb-3">
                                            <input type="text" class="form-control input-white mb-10" name="contact-name" required  placeholder="Enter Name" aria-label="Recipient's Name" aria-describedby="contact">

                                            <input type="tel" class="form-control input-white mb-10" name="contact-phone" placeholder="Enter phone number" aria-label="Recipient's Phone Number" aria-describedby="contact" >

                                            <input type="email" class="form-control input-white mb-10" name="contact-email" required placeholder="Enter email address" aria-label="Recipient's Email" aria-describedby="contact">

                                            <textarea class="form-control input-white mb-10" name="contact-message" required  placeholder="Enter a question or comment" aria-label="Enter a question or comment"></textarea>

                                            <button class="btn btn-square-blue mt-10" type="submit">Submit</button>
                                        
                                        </div>
                                    </form>
                                <?php endif ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- coming soon area end -->

    <!-- ********************* -->
    <!-- JS Files -->
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/main.js"></script>
    <!-- JS Files end -->
</body>

</html>